FROM composer:2@sha256:d702aa6a31321b7c2f7e4258334ec965c2813859a2db3617b8a9f746b44e42c2 AS composer
FROM php:8.2.27-cli-alpine@sha256:db9ce70a50c5274b8c7688dc0747e93a0b7bf4bcaeb861090d828f928a0a881e

COPY --from=composer /usr/bin/composer /usr/bin/

# hadolint ignore=DL3018,SC2086
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS libpng-dev~=1 zlib-dev~=1 linux-headers~=6 \
    && apk add --no-cache bash~=5 git~=2 openssh~=9 libpng~=1 \
    && docker-php-ext-install gd mysqli \
    && pecl install xdebug-3.4.0 \
    && docker-php-ext-enable gd mysqli xdebug \
    && apk del -f .build-deps \
    && composer global require phpunit/phpunit ^11

WORKDIR /app

ENV PATH="/root/.composer/vendor/bin/:${PATH}" \
    COMPOSER_ALLOW_SUPERUSER="1"

ENTRYPOINT []
CMD ["phpunit"]
